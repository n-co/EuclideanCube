package ecgraph;

/**
 * Created by avishay on 7/13/17.
 */
import heap.FibonacciHeap;
import heap.Heap.Entry;
public class FibonacciVertexHeap  {

    FibonacciHeap<Double, FibonacciHeapPriorityObject> heap = new FibonacciHeap<Double, FibonacciHeapPriorityObject>();

    public void add(PriorityObject item) {
        Entry<Double, FibonacciHeapPriorityObject> entry = heap.insert(item.priority, (FibonacciHeapPriorityObject)item);
        ((FibonacciHeapPriorityObject)item).entry = entry;
    }

    public void decreasePriority(PriorityObject item, double priority) {
        item.priority = priority;
        heap.decreaseKey(((FibonacciHeapPriorityObject)item).entry, priority);
    }

    public PriorityObject extractMin() {
        return heap.extractMinimum().getValue();
    }

    public void clear() {
        heap.clear();
    }

    public int size() {
        return heap.getSize();
    }
    public boolean isEmpty(){
        return heap.isEmpty();
    }

}