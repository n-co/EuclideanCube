package ecgraph;

import heap.Heap.Entry;

/**
 * Created by avishay on 7/13/17.
 */
public class FibonacciHeapPriorityObject extends PriorityObject {
    public Entry<Double, FibonacciHeapPriorityObject> entry;

    public FibonacciHeapPriorityObject(int node, double distance) {
        super(node, distance);
    }

}
