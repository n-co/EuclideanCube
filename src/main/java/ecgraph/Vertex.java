package ecgraph;

import java.util.LinkedList;

public class Vertex {

	public double dist;
	public LinkedList<Integer> neighbors;
	public int prev;
	
	public Vertex(){
		this.neighbors = new LinkedList<Integer>();
		this.dist = Double.MAX_VALUE;
		this.prev = -1;
	}
}
