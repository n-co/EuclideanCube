package ecgraph;


public class ECGraph {

	private final int DIM;
	private final int V_SIZE;
	private Vertex vertices[];
	private long edgesCounter;

	// for the dijkstra algorithm
	private FibonacciHeapPriorityObject[] priorityObjectsArray;
	private FibonacciVertexHeap verticesMinHeap;
	// dijkstra cache
	private boolean graphWasChanged;
	private int lastSource;

	public ECGraph(int dim){
		this.DIM = dim;
		this.V_SIZE = 1 << DIM;
		this.vertices = new Vertex[V_SIZE];
		this.edgesCounter = 0;
		this.priorityObjectsArray = new FibonacciHeapPriorityObject[V_SIZE];
		for(int i=0; i<V_SIZE; i++){
			vertices[i] = new Vertex();
			FibonacciHeapPriorityObject obj = new FibonacciHeapPriorityObject(i,vertices[i].dist);
			priorityObjectsArray[i] = obj;
		}
		this.verticesMinHeap = new FibonacciVertexHeap();
		this.graphWasChanged = true;
		this.lastSource = -1;

	}
	
	public void addEdge(int u, int v){
		vertices[u].neighbors.add(v);
		vertices[v].neighbors.add(u);
		graphWasChanged = true;
		edgesCounter++;
	}
	
	public double distance(int u, int v, int maxStretch){
		if(u != lastSource || graphWasChanged) {
			dijkstra(u, maxStretch);
		}
		return vertices[v].dist;
	}

	public long getEdgesCount(){
		return edgesCounter;
	}

	public int getDimension(){
		return DIM;
	}

	public static double edgeWeight(int u, int v) {
		int diff = u ^ v;
		//built-in function that calculate the number of bits that are 1 in integer.
		int distSquare = Integer.bitCount(diff);
		return Math.sqrt(distSquare);
	}

	private void dijkstra(int s, int maxStretch){

		initSingleSource(s);
		verticesMinHeap.clear();
		for(int v=0; v<V_SIZE; v++){
			//FibonacciHeapPriorityObject obj = new FibonacciHeapPriorityObject(v,vertices[v].dist);
			priorityObjectsArray[v].priority = vertices[v].dist;
			verticesMinHeap.add(priorityObjectsArray[v]);
		}
		Integer u_id;
		Vertex u;

		while(!verticesMinHeap.isEmpty()){

			PriorityObject minVertex = verticesMinHeap.extractMin();
			u_id = minVertex.node;
			u = vertices[u_id];
			//too far vertex was extracted, we can stop for sure.
			if(u.dist > maxStretch * edgeWeight(s, u_id))
				break;
			for(Integer v_id : u.neighbors){

				double relaxation = relax(u_id,v_id);
				if (relaxation != Double.MAX_VALUE && vertices[u_id].dist < vertices[v_id].dist)
					verticesMinHeap.decreasePriority(priorityObjectsArray[v_id],relaxation);
			}
		}
		lastSource = s;
		graphWasChanged = false;
	}
	
	private void initSingleSource(int s){
		for(Vertex v : vertices){
			v.dist = Double.MAX_VALUE;
			v.prev = -1;
		}
		vertices[s].dist = 0;
	}
	
	private double relax(int u_id, int v_id){
		Vertex u = vertices[u_id];
		Vertex v = vertices[v_id];
		double uvWeight = edgeWeight(u_id, v_id);
		if(v.dist > u.dist + uvWeight){
			v.dist = u.dist + uvWeight;
			v.prev = u_id;
		}
		return v.dist;
	}

	public void printPath(int s, int t, int maxStretch){
		dijkstra(t, maxStretch);
		int curr = s;
		double pathWeight = 0.0;
		StringBuilder path = new StringBuilder().append('[');
		while(curr != -1){
			path.append(curr).append(", ");
			int prev = vertices[curr].prev;
			if(prev != -1)
				pathWeight += edgeWeight(curr, prev);
			curr = prev;
		}
		path.setLength(path.length() - 2); // remove last 2 chars
		path.append("]");
		double stWeight = edgeWeight(s, t);

		System.out.println(path);
		System.out.println("path weight: " + pathWeight);
		System.out.println("edge weight: " + stWeight);
		System.out.println("stretch: " + (pathWeight/stWeight));
	}
}
