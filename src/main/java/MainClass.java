import ecgraph.ECGraph;
import generators.*;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.Random;

public class MainClass {

    private static final String xlsPath = "results.xls";
    private static XSSFWorkbook workbook = new XSSFWorkbook();
    private static XSSFSheet edgesSheet = workbook.createSheet("Sheet1");
    private static XSSFSheet timesSheet = workbook.createSheet("Sheet2");
    private static FileOutputStream f;

    // default arguments
    private static int minDim = 16;
    private static int maxDim = 16;
    private static int minK = 3;
    private static int maxK = Integer.MAX_VALUE;
    private static int stars = 1;


    public static void main(String[] args) throws Exception {

        readArguments(args);
        initExcel();
        for (int d = minDim; d<=maxDim; d++){
            //generate seed for each dimension
            long seed = new Random().nextLong();
            int kLimit = (int)Math.min(maxK, Math.ceil(Math.sqrt(d)));
            for (int k = minK; k<=kLimit; k++) {

                long startTime = System.currentTimeMillis();
                ResultPack result = runGreedy(d, k, seed);

                long endTime = System.currentTimeMillis();
                long totalTime = endTime - startTime;
                double edgesToDim = (double)(result.graph.getEdgesCount())/(Math.pow(2,d));
                String time = calculateTime(totalTime);
                writeToExcel(d, k,edgesToDim, seed, time);

                System.out.println("Total runtime: " + time);
            }
        }

    }

    /**
     * Run the greedy algorithm on euclidean cube clique, but first builds a star-graph around vertex 0,
     * for runtime optimization
     * @param dim the dimension of the cube
     * @param k the maximum stretch allowed
     * @param seed the random seed for a {@link RandomEdgesGenerator}
     * @return {@link ResultPack} containing the spanner and the seed, if exist
     */
    private static ResultPack runGreedy(int dim, int k, long seed) {
        System.out.printf("Looking for the greedySpanner for EC of dim: %d and k:%d\n", dim, k);
        int maxStretch = k;

        ECGraph graph = new ECGraph(dim);
        int[] edge;
        int u, v;
        EdgesGenerator edgeGen = new RandomEdgesGenerator(dim, seed);

        ResultPack result = new ResultPack();
        result.graph = graph;

        if (edgeGen instanceof Randomable){
            result.randomSeed = ((Randomable) edgeGen).getSeed();
        }

        double maxDist = Double.MAX_VALUE;
        int lastVertex = (1<<dim) - 1;
        // set maximum edge distance to check, and build default spanner according to "stars" argument
        if(stars == 1){
            buildDefaultStar(graph);
            maxDist = 2*Math.sqrt(dim)/maxStretch;
        }
        else if (stars == 2){
            buildDefaultStar(graph);
            buildDefaultSecondStar(graph);
            maxDist = Math.sqrt(2*dim)/maxStretch;
        }

        while (edgeGen.hasNext()) {
            edge = edgeGen.next();
            u = edge[0];
            v = edge[1];

            // when we are using default star-spanners
            switch (stars){
                case 2:
                    if(v == lastVertex) // edge already belongs to spanner because of second star-graph
                        continue;
                case 1:
                    if(u == 0) // edge already belongs to spanner because of star-graph
                        continue;
                case 0:
                    // NOTHING
            }

            double dist = ECGraph.edgeWeight(u, v);

            if(dist >= maxDist) // there is already good enough stretch because of star-graph(s)
                break;

            double graphDist = graph.distance(u, v, maxStretch);
            if (maxStretch * dist < graphDist) {
                graph.addEdge(u, v);
                System.out.printf("(%4d,%4d)  %f %10d    %f\n", u, v, dist, graph.getEdgesCount(), edgeGen.getProgress());
            }

        }
        return result;
    }

    private static void buildDefaultStar(ECGraph graph) {
        int dim = graph.getDimension();
        int vertices = (1 << dim);

        for(int v = 1; v<vertices; v++){
            graph.addEdge(0, v);
        }
    }

    private static void buildDefaultSecondStar(ECGraph graph) {
        int dim = graph.getDimension();
        int vertices = (1 << dim);
        int v = vertices - 1;

        for(int u = 1; u<v; u++){
            graph.addEdge(u, v);
        }
    }
    private static String calculateTime(long millis) {

        long sec = millis / 1000;
        long min = sec / 60;
        long hours = min / 60;
        sec %= 60;
        min %= 60;
        millis %= 1000;
        return String.format("%02d:%02d:%02d.%03d", (int) hours, (int) min, (int) sec, (int) millis);
    }

    /**
     *
     * Write the data about the greedy spanner to excel
     *
     * @param d - the dimension
     * @param k - the k of the greedySpanner
     * @param EdgesVerticesRatio - the ratio between the edges and the vertices of the greedySpanner
     * @param seed - The seed for the given dimension
     * @throws Exception
     */
    private static void writeToExcel(int d, int k, double EdgesVerticesRatio,long seed,String time) throws Exception {

        String str = "" + EdgesVerticesRatio;

        updateValueInSheet(d, k, str,edgesSheet);
        updateValueInSheet(d,k,time,timesSheet);
        XSSFRow sheetRow;
        Cell cell;

        //write the seed.
        int seedCol = getMaxK()+1;
        sheetRow = edgesSheet.getRow(d-2);
        cell = sheetRow.getCell(seedCol);
        if(cell==null)
            cell = sheetRow.createCell(seedCol);
        cell.setCellValue((int)seed);

        f = new FileOutputStream(new File(xlsPath));
        workbook.write(f);
        f.flush();
        //f.close();
    }

    private static void updateValueInSheet(int d, int k, String str,XSSFSheet sheet) {
        Cell cell;
        XSSFRow sheetRow = sheet.getRow(d-2);
        if (sheetRow == null) {
            sheetRow = sheet.createRow(d-2);
        }
//Update the value of cell
        cell = sheetRow.getCell(k-1);
        if (cell == null) {
            cell = sheetRow.createCell(k-1);
        }
        cell.setCellValue(str);
    }

    /**
     * Init the excel file.
     * @throws IOException
     */
    private static void initExcel() throws IOException {

        int k = getMaxK();
        initHeaders(k,edgesSheet);
        initHeaders(k,timesSheet);
        Row row;
        Cell cell;
        row = edgesSheet.getRow(0);
        cell = row.createCell(k+1);
        cell.setCellValue("Seed");


        f = new FileOutputStream(new File(xlsPath));
        workbook.write(f);
        f.flush();
        //f.close();
    }

    private static void initHeaders(int k,XSSFSheet sheet) {
        Row row = sheet.createRow(0);
        Cell cell = row.createCell(0);
        cell.setCellValue("D\\K");
        for (int i = 1; i <= maxDim; i++) {
            row = sheet.createRow(i);
            cell = row.createCell(0);
            cell.setCellValue(i+2);
        }
        for (int i = 1; i <= k; i++) {
            row = sheet.getRow(0);
            cell = row.createCell(i);
            cell.setCellValue(i+1);
        }
    }

    private static int getMaxK() {
        return (int)Math.ceil(Math.sqrt(maxDim));
    }

    private static void readArguments(String[] args) {
        try{
            for (int i = 0; i < args.length; i++) {
                String arg = args[i];
                if (arg.equals("-d")) {
                    minDim = Integer.parseInt(args[++i]);
                    maxDim = Integer.parseInt(args[++i]);
                } else if (arg.equals("-k")) {
                    minK = Integer.parseInt(args[++i]);
                    if (args.length > i + 1 && args[i + 1].matches("\\d+")) // check if next argument is available and an int
                        maxK = Integer.parseInt(args[++i]);
                } else if (arg.equals("-stars")) {
                    stars = Integer.parseInt(args[++i]);
                }
            }
        }catch(NumberFormatException e){
            throw new IllegalArgumentException("argument must be an integer");
        }catch(IndexOutOfBoundsException e){
            throw new IllegalArgumentException("not enough arguments were entered");
        }
        if (minDim > maxDim) {
            throw new IllegalArgumentException("minimum dimension can't be greater than maximum dimension");
        }
        if (minK > maxK) {
            throw new IllegalArgumentException("minimum k can't be greater than maximum k");
        }
        if (stars < 0 || stars > 2) {
            throw new IllegalArgumentException("there can be only 0, 1 or 2 stars");
        }
        if (minDim < 0){
            throw new IllegalArgumentException("dimension can't be negative");
        }
        if (minK <= 0){
            throw new IllegalArgumentException("k must be positive");
        }
    }
    private static class ResultPack{
        public ECGraph graph = null;
        public long randomSeed = 0;
    }
}


