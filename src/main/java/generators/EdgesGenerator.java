package generators;

import java.util.NoSuchElementException;

/**
 * a generator that generates weighted edges of euclidean cube clique in ascending order.
 */
public abstract class EdgesGenerator implements Generator<int[]>{

	protected final int DIM;
	protected VerticesGenerator vGen;
	protected XorDistanceGenerator xorGen;
	protected int distCounter; // the current distance of generated edges. starting from 1 to "DIM"
	protected int[] edge;
	protected long progressCounter;


	protected int v1;
	protected long givenEdgesCounter;
	protected final long TOTAL_EDGES;


	public EdgesGenerator(int dim) {
		this.DIM = dim;
		this.TOTAL_EDGES = (1L << (2*DIM-1)) - (1L << (DIM-1)); // [(2^d)*((2^d)-1)]/2
		this.edge = new int[2];
		// create new VerticesGenerator in non-abstract class
		// call to reset in non-abstract class
	}
	
	@Override
	public void reset() {
		vGen.reset();
		distCounter = 1;
		givenEdgesCounter = 0;
		v1 = vGen.next();
		progressCounter = 0;
		// create new XorDistanceGenerator in non-abstract class
	}

	@Override
	public boolean hasNext() {
		return givenEdgesCounter < TOTAL_EDGES;
	}

	/**
	 * get the next most light-weighted edge of an euclidean cube clique. <br>
	 * gives priority to edges that share their "left" vertex with last generated edge.
	 * @return next edge (v1, v2) such that v1 < v2
	 */
	@Override
	public int[] next() {
		int v2, xor;
		if(!hasNext())
			throw new NoSuchElementException();
		do{
			progressCounter++;
			if(xorGen.hasNext()){
				xor = xorGen.next();
				v2 = v1 ^ xor;
			}
			else if (vGen.hasNext()){
				xorGen.reset();
				xor = xorGen.next();
				v1 = vGen.next();
				v2 = v1 ^ xor;
			}
			else if (distCounter < DIM){
				distCounter++;
				xorGen = new XorDistanceGenerator(distCounter, DIM);
				vGen.reset();
				xor = xorGen.next();
				v1 = vGen.next();
				v2 = v1 ^ xor;
			}
			else{
				throw new NoSuchElementException("oops... this is shouldn't happen");
			}
		}while(v1 >= v2);
		edge[0] = v1;
		edge[1] = v2;
		givenEdgesCounter++;
		return edge;
	}

	@Override
	public double getProgress() {
		// the generator potentially iterate every edge twice (for each vertex)
		// therefore, the progress is the ratio between all passed iterations to TOTAL_EDGES*2;
		return ((double)progressCounter)/(TOTAL_EDGES << 1);
	}

	@Override
	public long size() {
		return TOTAL_EDGES;
	}

}
