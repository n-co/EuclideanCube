package generators;

public interface Generator<T> {

	/** reset the generator to it's starting point */
	public void reset();
	public boolean hasNext();
	public T next();
	/**
	 * get the relative progress of the generator
	 * @return progress level between 0 to 1
	 */
	public double getProgress();
	public long size();
}
