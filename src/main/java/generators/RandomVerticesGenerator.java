package generators;

import java.util.NoSuchElementException;
import java.util.Random;

public class RandomVerticesGenerator extends VerticesGenerator implements Randomable{

	private int jumpGap;
	private int zero;
	private Random rand;
	private final long RAND_SEED;


	public RandomVerticesGenerator(int dimension, long seed) {
		super(dimension);
		this.RAND_SEED = seed;
		this.rand = new Random(RAND_SEED);
		reset();
	}

	public RandomVerticesGenerator(int dimension) {
		this(dimension, new Random().nextLong());
	}
	
	@Override
	public void reset(){
		zero = (int)(rand.nextDouble() * TOTAL_VERTICES);
		jumpGap = (int)(rand.nextDouble() * TOTAL_VERTICES);
		jumpGap |= 1; // make it odd number
		jumpGap %= TOTAL_VERTICES;
		current = -1;
		progressCounter = 0;
	}
	
	@Override
	public boolean hasNext() {
		return progressCounter < TOTAL_VERTICES;
	}
	
	@Override
	public Integer next(){
		if(!hasNext())
			throw new NoSuchElementException();
		
		if(current == -1){
			current = zero;
		}
		else{
			current += jumpGap;
			current %= TOTAL_VERTICES;
		}
		progressCounter++;
		return current;
	}

	@Override
	public long getSeed(){
		return RAND_SEED;
	}

}
