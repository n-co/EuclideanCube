package generators;

import java.util.NoSuchElementException;

public class SerialVerticesGenerator extends VerticesGenerator{

	public SerialVerticesGenerator(int dimension) {
		super(dimension);
		reset();
	}
	
	@Override
	public void reset() {
		progressCounter = 0;
		current = -1;
	}

	@Override
	public boolean hasNext() {
		return current < TOTAL_VERTICES - 1;
	}

	@Override
	public Integer next() {
		if(!hasNext())
			throw new NoSuchElementException();
		progressCounter++;
		return ++current;
	}

}
