package generators;

public class SerialEdgesGenerator extends EdgesGenerator{


	/**
	 * a generator that generates weighted edges of euclidean cube clique in ascending order.
	 * @param dim the dimension of the euclidean cube
	 */
	public SerialEdgesGenerator(int dim) {
		super(dim);
		this.vGen = new SerialVerticesGenerator(DIM);
		reset();
	}
	
	@Override
	public void reset() {
		super.reset();
		xorGen = new XorDistanceGenerator(distCounter, DIM);
	}

	@Override
	public boolean hasNext() {
		return givenEdgesCounter < TOTAL_EDGES;
	}

}
