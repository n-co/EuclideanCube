package generators;

public abstract class VerticesGenerator implements Generator<Integer> {
	
	protected final int DIM;
	protected final int TOTAL_VERTICES;
	protected int current;
	protected int progressCounter;
	private int toStringDelta;
	
	public VerticesGenerator(int dimension) {
		DIM = dimension;
		TOTAL_VERTICES = (1 << DIM);
		toStringDelta = 1+(int)Math.log10(TOTAL_VERTICES);
	}
	
	@Override
	public String toString(){
		return String.format("%"+toStringDelta+"d", current);
	}

	@Override
	public double getProgress() {
		return ((double) progressCounter)/ TOTAL_VERTICES;
	}
	
	@Override
	public long size(){
		return TOTAL_VERTICES;
	}

}
