package generators;

import java.util.NoSuchElementException;

/**
 * This generator defined by two values - dimension (dim) and distance (dist).
 * The generator generates all the binary numbers such that their length is "dim", and contains exactly "dist" ones
 */
public class XorDistanceGenerator implements Generator<Integer>{

	private final int DIST;
	private final int DIM;
	private final int MAX_VALUE;
	private final int MIN_VALUE;
	private final long SIZE;
	private int num;
	private int pointer;
	private int bitsCounter;
	private int progressCounter;
	
	public XorDistanceGenerator(int distance, int dimension){
		if(distance <= 0 || dimension <= 0)
			throw new RuntimeException("distance and dimension must be positive");
		if(distance > dimension)
			throw new RuntimeException("distance can't be greater than dimension");
		
		this.DIST = distance;
		this.DIM  = dimension;
		this.MIN_VALUE = (1 << DIST) - 1;
		this.SIZE = nChooseK(DIM, DIST);
		this.MAX_VALUE = MIN_VALUE << (DIM - DIST);
		reset();
	}
	
	@Override
	public String toString(){
		String binStr = Integer.toBinaryString(num);
		return String.format("%"+DIM+"s", binStr);
	}

	/**
	 * put "zeroLength" zeros in the rightmost part of the the number, <br>
	 * then put "onesLength" ones, in the rightmost part of the number
	 * @param onesLength
	 * @param zeroLength
	 */
	private void resetLSBs(int onesLength, int zeroLength) {
		num &= ((-1) << zeroLength); // put zeros
		num |= ((1 << onesLength) - 1); // put ones
	}
	
	@Override
	public void reset(){
		progressCounter = 0;
		num = 0;
		bitsCounter = DIST;
		pointer = DIST;
	}
	
	@Override
	public boolean hasNext(){
		return num < MAX_VALUE;
	}

	@Override
	public Integer next(){
		if(!hasNext())
			throw new NoSuchElementException();
		if(num == 0){
			num = MIN_VALUE;
			return num;
		}
		while(true){
			if((num & (1<<pointer)) == 0){
				
				if(bitsCounter > 0){
					// possible to move bit to the left
					resetLSBs(bitsCounter-1, pointer);
					num |= (1 << pointer);
					bitsCounter--;
					pointer = bitsCounter;
					break;
				}
			}
			else{
				bitsCounter++;
			}
			pointer++;
		}
		progressCounter++;
		return num;
	}

	@Override
	public double getProgress() {
		return ((double)progressCounter)/SIZE;
	}

	@Override
	public long size() {
		return SIZE;
	}
	
	private long nChooseK(int n, int k){
		long size = 1;
		k = Math.max(k, n-k);
		for(int i=k+1; i<=n; i++){ //calculates n!/k!
			size*=i;
		}
		for(int i=1; i<=n-k; i++){ //divide with (n-k)!
			size /= i;
		}
		
		return size;
	}
	
}
