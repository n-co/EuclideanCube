package generators;

import java.util.Random;

public class RandomEdgesGenerator extends EdgesGenerator implements Randomable{

	private Random rand;
	private final long RAND_SEED;

	/**
	 * a generator that generates weighted edges of euclidean cube clique in ascending order.
	 * @param dim the dimension of the euclidean cube
	 * @param seed the pseudo-random randomSeed
	 */
	public RandomEdgesGenerator(int dim, long seed) {
		super(dim);
		this.RAND_SEED = seed;
		this.rand = new Random(RAND_SEED);
		this.vGen = new RandomVerticesGenerator(DIM, rand.nextLong());
		reset();
	}

	/**
	 * a generator that generates weighted edges of euclidean cube clique in ascending order.
	 * @param dim the dimension of the euclidean cube
	 */
	public RandomEdgesGenerator(int dim) {
		this(dim, new Random().nextLong());
	}
	
	@Override
	public void reset() {
		super.reset();
		xorGen = new XorDistanceGenerator(distCounter, DIM);
	}

	@Override
	public long getSeed() {
		return RAND_SEED;
	}
}
