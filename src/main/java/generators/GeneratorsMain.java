package generators;

public class GeneratorsMain {

	public static void main(String[] args) {

		XorDistanceGenerator xGen = new XorDistanceGenerator(3,5);
		while(xGen.hasNext()){
			xGen.next();
			System.out.println(xGen);
		}
		printEdges();
		
	}

	private static double distance(int v1, int v2){
		int diff = v1 ^ v2;
		int distSquare = Integer.bitCount(diff);
		return Math.sqrt(distSquare);
	}
	
	private static double distance(int[] edge){
		return distance(edge[0], edge[1]);
	}
	
	private static void printEdges(){
		SerialEdgesGenerator eGen1 = new SerialEdgesGenerator(5);
		RandomEdgesGenerator eGen2 = new RandomEdgesGenerator(5);
		while(eGen1.hasNext()){
			int[] e1 = eGen1.next();
			int[] e2 = eGen2.next();
			System.out.printf("(%3s, %3s) %f      ", e1[0], e1[1], distance(e1));
			System.out.printf("(%3s, %3s) %f\n", e2[0], e2[1], distance(e2));
		}
	}

}
