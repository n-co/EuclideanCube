package generators;

public interface Randomable {

    public long getSeed();

}
